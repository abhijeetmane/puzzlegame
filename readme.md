# Project Title

Sliding Puzzle to complete image by reshuffling image blocks.

# Project Description

This is puzzle where you can complete image by sliding image blocks.

# Tech Stack

Application uses:

* [ES6]
* [HTML]
* [CSS]

# Start Application

```
$ Open index.html in browser
```

# To Do

- Provide slider to zoom in and zoom out for small grids
- Keep limit on number of moves.
- Upload new image
- Solve puzzle automatically when user give up